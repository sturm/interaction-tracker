(ns sturm.interactions.core
  (:require
    [clojure.tools.logging :as log]
    [integrant.core :as ig]
    [sturm.interactions.config :as config]
    [sturm.interactions.env :refer [defaults]]

    ;; Edges       
    [kit.edge.server.undertow]
    [sturm.interactions.web.handler]

    ;; Routes
    [sturm.interactions.web.routes.api]
    
    [sturm.interactions.web.routes.pages]
    [kit.edge.db.sql.conman] 
    [kit.edge.db.sql.migratus])
  (:gen-class))

;; log uncaught exceptions in threads
(Thread/setDefaultUncaughtExceptionHandler
  (reify Thread$UncaughtExceptionHandler
    (uncaughtException [_ thread ex]
      (log/error {:what :uncaught-exception
                  :exception ex
                  :where (str "Uncaught exception on" (.getName thread))}))))

(defonce system (atom nil))

;; Used in test system-fixture fn and in start-app.
(defn stop-app []
  ((or (:stop defaults) (fn [])))
  (some-> (deref system) (ig/halt!))
  (shutdown-agents))

;; Params used for test system-fixture.
(defn start-app [& [params]]
  ((or (:start params) (:start defaults) (fn [])))
  (->> (config/system-config (or (:opts params) (:opts defaults) {}))
       (ig/prep)
       (ig/init)
       (reset! system))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app)))

(defn -main [& _]
  (start-app))
