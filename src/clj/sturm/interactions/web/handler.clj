(ns sturm.interactions.web.handler
  "Initialise the routes and ring handler.
  These functions are called by Integrant during (ig/init)."
  (:require
    [sturm.interactions.web.middleware.core :as middleware]
    [sturm.interactions.web.pages.layout :refer [error-page]]
    [integrant.core :as ig]
    [reitit.middleware]
    [reitit.ring :as ring]
    [reitit.swagger-ui :as swagger-ui]))

(defmethod ig/init-key :router/routes
  [_ {:keys [routes]}]
  (apply conj [] routes))

(defmethod ig/init-key :router/core
  ;; Are there every keys other than :routes?
  [_ {:keys [routes] :as opts}]
  (ring/router
   ["" opts routes]
   ;; Enable to see what each middleware does.
   #_{:reitit.middleware/transform dev/print-request-diffs}))

(defmethod ig/init-key :handler/ring
  [_ {:keys [router api-path] :as opts}]
  (ring/ring-handler
   router
   (ring/routes
    ;; Handle trailing slash in routes - add it + redirect to it
    ;; https://github.com/metosin/reitit/blob/master/doc/ring/slash_handler.md
    (ring/redirect-trailing-slash-handler)
    (ring/create-resource-handler {:path "/"})
    (when (some? api-path)
      (swagger-ui/create-swagger-ui-handler {:path api-path
                                             :url  (str api-path "/swagger.json")}))
    (ring/create-default-handler
     ;; FEATURE: Custom 404 error page
     {:not-found
      (constantly (error-page {:status 404 :title "Not found" :message "Sorry, that page was not found"}))
      :method-not-allowed
      (constantly (error-page {:status 405 :title "Not allowed" :message "Sorry, that request method is not supported"}))
      :not-acceptable
      (constantly (error-page {:status 406 :title "Not acceptable" :message "Sorry, that request was not acceptable"}))}))
   {:middleware [(middleware/wrap-base opts)]}))
