(ns sturm.interactions.web.controllers.interactions
  (:require
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [clojure.tools.logging :as log]
   [cuerdas.core :as str]
   [hiccup2.core :as h2]
   [hiccup.form :as f]
   [next.jdbc.date-time]  ; Auto-convert dates to SQL strings and vice versa
   [ring.util.anti-forgery :refer [anti-forgery-field]]
   [ring.util.http-response :as http]
   [struct.core :as st]
   [sturm.interactions.web.routes.utils :as utils]
   [sturm.interactions.web.pages.layout :as layout]))

;;; Sean Corfield's REPL Driver Development tips
;;; https://www.youtube.com/watch?v=gIoadGfm5T8
;;
;; Socket REPL is simple and works well.
;;
;; Explore a namespace (perhaps via hotkey):
;; (ns-publics (find-ns 'ring.util.http-response))
;;
;; Key for eval top-level form, eg. cider-eval-defun-at-point (C-M-x). Maybe I
;; should rebind to C-c C-d or C-x C-d.
;;
;; Use (comment) blocks for experiments and #_ to disable forms.
;;
;; (doto tap> _) to tap and return value.
;;
;; Key for running this namespace's tests from editor.

(def default-interaction {:duration 15})

(defn- bucket-clients
  "Group clients into a fixed number of buckets based on a numeric key.
  Used to produce a basic chart of client value."
  [clients key num-buckets]
  (let [clients-sorted (->> clients
                            ;; Convert nil values to zero so we can sort/group
                            (map #(if (nil? (key %)) (assoc % key 0) %))
                            (sort-by key)
                            reverse)
        max-value (->> clients-sorted
                       (apply max-key key)
                       key)
        empty-buckets (into (sorted-map) (zipmap (range num-buckets) (repeat [])))]
    (->> clients-sorted
         (group-by #(quot (key %) (/ max-value (dec num-buckets))))
         (into empty-buckets))))

(defn or-zero-fn [k]
  (fn [m]
    (if (nil? (k m)) 0 (k m))))

(defn client-overview
  "List all clients, their total interactions and their revenue.
  Displays any errors from an unsuccessful call to `save-client!`."
  [{:keys [flash] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        clients (query-fn :get-clients {})
        year-turnover (->> clients
                           (map :year_turnover)
                           (filter number?)
                           (reduce +))
        {active-clients true inactive-clients false} (group-by :active clients)
        active-clients (sort-by (or-zero-fn :year_turnover) > active-clients)
        inactive-clients (sort-by (or-zero-fn :lifetime_value) > inactive-clients)

        clients-by-year-turnover (bucket-clients (filter (comp pos? (or-zero-fn :year_turnover)) clients) (or-zero-fn :year_turnover) 100)
        clients-by-lifetime-value (bucket-clients (filter (comp pos? (or-zero-fn :lifetime_value)) clients) (or-zero-fn :lifetime_value) 100)
        recent-interactions (query-fn :recent-interactions {})]
    (layout/render request "client_overview.html" {:year-turnover year-turnover
                                                   :active-clients active-clients
                                                   :inactive-clients inactive-clients
                                                   :clients-by-year-turnover clients-by-year-turnover
                                                   :clients-by-lifetime-value clients-by-lifetime-value
                                                   :recent-interactions recent-interactions
                                                   :errors (:errors flash)})))

(def client-schema
  [[:name st/required st/string]])

(defn save-client!
  "Create a new client."
  [{:keys [params] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        [errors data] (st/validate params client-schema {:strip true})]
    (if errors
      ;; Unlike typical Django approach, validation errors will 302 redirect
      ;; back to the form. It's not obvious what happened from the HTTP status
      ;; codes, only the text on the page.
      (-> (http/found "/")
          (assoc-in [:flash :errors] errors))
      (do
        (log/debug "saving client" (:name data))
        (query-fn :save-client! data)
        (http/found "/")))))

(defn action [current potential]
  (if (or (str/blank? current) (str/blank? potential))
    nil
    (case [(keyword current) (keyword potential)]
      [:high :high] :promote
      [:high :medium] :retain
      [:high :low] :retain
      [:medium :high] :grow
      [:medium :medium] :retain
      [:medium :low] :retain
      [:low :high] :grow
      [:low :medium] :grow
      [:low :low] :manage)))

(defn view-client
  [request]
  (let [{:keys [query-fn]} (utils/route-data request)
        client-id (get-in request [:parameters :path :id])
        client (query-fn :get-client {:id client-id})
        interactions (query-fn :get-interactions {:client-id client-id})
        total-interactions-annual (:count (query-fn :total-interactions-annual {:client-id client-id}))
        add-url (str "/add-interaction/" client-id)
        {:keys [current_value potential_value]} client
        action (action current_value potential_value)
        meters-set (not= (into #{} ((juxt :rating_people :rating_profit :rating_culture :rating_values) client)) #{3})
        per-interaction (if-not (zero? total-interactions-annual)
                          (quot (:year_turnover client) total-interactions-annual)
                          0)]
    (layout/render request "client_detail.html" {:title (:name client)
                                                 :client client
                                                 :total-interactions-annual total-interactions-annual
                                                 :per-interaction per-interaction
                                                 :meters-set meters-set
                                                 :action action
                                                 :interactions interactions
                                                 :form-data default-interaction
                                                 :form-action-url add-url})))

(defn interaction-list-fragment
  [{:keys [flash params] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        client-id (str/parse-int (:client_id params))
        query (or (:q params) "")
        searching? (not (str/empty? query))
        interactions (if searching?
                       (query-fn :search-interactions-by-client {:client-id client-id
                                                                 :query query})
                       (query-fn :get-interactions {:client-id client-id}))]
    (layout/render request "_interaction_list.html" {:interactions interactions
                                                     :client_id client-id
                                                     :query query
                                                     :open (:open flash)})))

(defn site-search-fragment
  [{:keys [params] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        query (or (:q params) "")
        searching? (not (str/empty? query))
        interactions (if searching?
                       (query-fn :search-interactions {:query query})
                       [])]
    (layout/render request "_site_search.html" {:query query
                                                :interactions interactions})))


(defn- timestamp?
  [s]
  (try
    (.parse (java.text.SimpleDateFormat. "yyyy-MM-dd'T'HH:mm") s)
    (catch Exception _
      false)))

(def timestamp
  {:message "must be a timestamp"
   :optional true
   :validate timestamp?
   :coerce timestamp?})

(defn default
  "Provide a default value `d` for form fields that can be left blank/unchecked."
  [d]
  {:coerce (fn [x] (if (nil? x) d x))})

(def interaction-schema
  [[:timestamp timestamp]
   [:subject st/required st/string]
   [:medium st/string]
   [:duration st/required st/integer-str st/positive]
   [:details st/string]
   [:internal-notes st/string]
   ;; Checkboxes need value="true" set to comply with this.
   [:followup-required st/boolean-str (default false)]])

(defn- datetime->local-time-string
  [date]
  (-> date
      ;; The timestamp *seems* to be stored in local time when I check with
      ;; `psql`, but the value returned from the query is converted to UTC. I'm
      ;; not sure how it knows to do that. The next.jdbc docs recommend storing
      ;; in UTC without a timezone.
      .toLocalDateTime
      (.format (java.time.format.DateTimeFormatter/ofPattern "yyyy-MM-dd HH:mm"))))

(defn- interaction->form-data
  "An HTML input field of `type=\"number\"` can't handle milliseconds.
  (Milliseconds come from the default current time adding an interaction.)"
  [interaction]
  (tap> interaction)
  (update interaction :timestamp datetime->local-time-string))

(defn- kebab-map
  "SQL queries returns keyword keys with underscores - fixes that.
  I wonder if there's a way to do this automatically?"
  [m]
  (cske/transform-keys csk/->kebab-case-keyword m))

(defn add-interaction!
  ;; It's a bit confusing that there's :form-parameters, :params
  ;; *and* :parameters.
  [{:keys [params request-method] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        client-id (get-in request [:parameters :path :client-id])
        add-url (str "/add-interaction/" client-id)]
    (if (= request-method :post)
      (let [[errors data] (st/validate params interaction-schema {:strip true})]
        (if-not errors
          (let [[{interaction-id :id}] (query-fn :save-interaction! (assoc data :client-id client-id))]
            (-> (layout/render request "_interaction_form.html" {:form-data default-interaction
                                                                 :form-action-url add-url})
                (assoc-in [:flash :open] interaction-id)
                (http/header "HX-Trigger" "reloadInteractionList")))
          (layout/render request "_interaction_form.html" {:form-data params
                                                           :errors errors
                                                           :form-action-url add-url
                                                           :cancel-url add-url})))
      ;; It's a little awkward that we both render this form in `client-detail`
      ;; as well as here with HTMX.
      (layout/render request "_interaction_form.html" {:form-data default-interaction
                                                       :form-action-url add-url}))))

;; It's frustrating that the add/update functions have such a similar shape, but
;; I'm not sure there's an obviously better way. Kit/Luminus examples separate
;; out the HTTP methods and when there's an error, pass form data around in the
;; flash session, but I don't know that this is significantly better, especially
;; if you're returning only components of the page with HTMX. I should try it.
;;
;; It really just reminds me that what we're doing here *isn't* simple, even
;; though Django makes it seem so. The Clojure code certainly *looks* more
;; complicated than the equivalent Django code, but I think that's just
;; superficial. Take the HTML template - it's really just moving a complicated
;; chunk out of sight.
;;
;; HTMX is a tradeoff too. It doesn't necessarily remove the complexity of the
;; application state (that you have to manage in a server-side page or SPA), but
;; just moves it into the browser. If there are a few interactions going on,
;; it's trivial to make a mistake and put the application into a broken
;; state. JavaScript certainly sucks away far more of everyone's life-force than
;; it deserves though, so I don't want to be too harsh on HTMX.
;;
;; I guess I'm more critical of this complexity because I bear the full
;; responsibility on behalf of my clients. I've just had the satisfaction (and
;; responsibility) of guiding a whole range of organisations and their systems
;; through the full lifecycle from inception to reality, through to being
;; supported and maintained and extended for more than a decade. All the
;; planning, architectural design, usability/user-experience design, quoting,
;; scope management, politics, teamwork, business-development, technology
;; choice, hosting, testing, security, monitoring, documentation, performance,
;; scaling, human errors, [NEED WORD: sustainable handover between
;; staff/maintaining organisational knowledge], outages, disaster recovery,
;; phone and email support, updates and evolution, integration with other
;; systems, reporting, troubleshooting problems, technology change, ongoing cost
;; management, handover to other suppliers and eventual retirement. A signifiant
;; amount of what I do requires deep technical experience and problem-solving,
;; but there's a lot more than just that. Perhaps more than many internal
;; developers get to deal with.
;;
;; As a consulting there's a freedom to remain associated with a project while
;; doing a range of other work. You don't necessarily have to change
;; employers (and lose touch with old projects) to keep things interesting.

(st/validate {:foo "true"} [[:foo st/boolean-str]])
(defn view-update-interaction!
  [{:keys [params request-method] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        interaction-id (get-in request [:parameters :path :id])
        interaction (kebab-map (query-fn :get-interaction {:id interaction-id}))
        update-url (str "/interaction/" interaction-id)
        add-url (str "/add-interaction/" (:client-id interaction))]
    (if (= request-method :post)
      (let [[errors data] (st/validate params interaction-schema {:strip true})]
        (if (nil? errors)
          (do
            (query-fn :update-interaction! (assoc data :id interaction-id))
            (-> (layout/render request "_interaction_form.html" {:form-data default-interaction
                                                                 :form-action-url add-url})
                (assoc-in [:flash :open] interaction-id)
                (http/header "HX-Trigger" "reloadInteractionList")))
          (layout/render request "_interaction_form.html" {:form-data params
                                                           :updating true
                                                           :errors errors
                                                           :form-action-url update-url
                                                           :cancel-url add-url})))
      (layout/render request "_interaction_form.html" {:form-data (interaction->form-data interaction)
                                                       :updating true
                                                       :form-action-url update-url
                                                       :cancel-url add-url}))))

(defn- date?
  [s]
  (try
    (.parse (java.text.SimpleDateFormat. "yyyy-MM-dd") s)
    (catch Exception _
      false)))

(def date
  {:message "must be a date"
   :optional true
   :validate #(or (empty? %) (date? %))
   :coerce #(if (empty? %) nil (date? %))})

;; Messy but work for now.
(def non-negative
  {:message "must be negative"
   :optional true
   :validate #(or (and (number? %) (not (neg? %))) (empty? %) )})

(def integer-str-or-blank
  {:message "must be a long"
   :optional true
   :validate #(or (number? %) (empty? %) (and (string? %) (str/numeric? %)))
   :coerce #(if (number? %) (int %) (if (empty? %) nil (str/parse-int %)))})

;; TODO: Requirement to explicitly add a new field to the save query is a pain.
(def client-schema2
  [[:name st/required st/string]
   [:short_name st/string (default "")]
   [:entity_name st/string (default "")]
   [:abn st/string (default "")]
   [:active st/boolean-str (default false)]
   [:commenced date (default nil)]
   [:summary st/string]
   [:background st/string]
   [:people st/string]
   [:service st/string]
   [:technology st/string]
   [:service_agreement st/boolean-str (default false)]
   [:retainer st/boolean-str (default false)]
   [:risky st/boolean-str (default false)]
   [:pro_bono st/boolean-str (default false)]
   [:non_financial st/boolean-str (default false)]
   [:current_value st/string]
   [:potential_value st/string]
   [:rating_people st/integer-str [st/in-range 1 5]]
   [:rating_profit st/integer-str [st/in-range 1 5]]
   [:rating_culture st/integer-str [st/in-range 1 5]]
   [:rating_values st/integer-str [st/in-range 1 5]]
   [:year_turnover integer-str-or-blank non-negative]
   [:lifetime_value integer-str-or-blank non-negative]])

(defn text-field [name label value errors]
  [:div.mb-2
   (f/label {:required true :class "block"} name (str label " "))
   (f/text-field {:class "px-1 border-2 border-slate-300 rounded w-full"} name value)
   (when errors [:div.notification.is-danger errors])])

(defn textarea-field [name label value errors height]
  [:div.mb-2
   (f/label {:class "block"} name (str label " "))
   (f/text-area {:class "px-1 border-2 border-slate-300 rounded w-full"
                 :style {:width "100%" :height height}}
                name value)
   (when errors [:div.notification.is-danger errors])])

(defn checkbox-field [name label checked errors]
  [:div.mb-2
   (f/check-box name checked)
   (f/label name (str " " label))
   (when errors [:div.notification.is-danger errors])])

(defn date-field [name label value errors]
  [:div.mb-2
   (f/label {:class "block"} name label)
   (f/text-field {:type "date"
                  :class "px-1 border-2 border-slate-300 rounded w-full"}
                 name value)
   (when errors [:div.notification.is-danger errors])])

(defn range-field [name label value errors]
  [:div.mb-2
   (f/label {:class "block"} name (str label " "))
   [:input {:name name :type "range" :min 1 :max 5 :value value}]
   (when errors [:div.notification.is-danger errors])])

(defn html-view [body]
  (-> (h2/html
          [:html {:xmlns "http://www.w3.org/1999/xhtml"}
           [:head
            [:meta {:charset "UTF-8"}]
            [:link {:href "/output.css" :rel "stylesheet"}]
            [:link {:href "/css/screen.css" :rel "stylesheet"}]]
           [:body {:class "text-slate-700 px-2 sm:px-4 py-4"}
            body
            ]])
      str
      http/ok
      (http/content-type "application/xhtml+xml")))

(defn edit-client-view
  [client-id client errors]
  ;; When I'd spelled out all the fields here literally I hit a "method too
  ;; long" error. https://github.com/weavejester/hiccup/issues/157
  (html-view
   [:form {:method :post
           :action (str "/client/" client-id "/edit")
           :style {:max-width "45rem"}
           :onkeyup "if (event.ctrlKey && event.key === 'Enter') { this.reportValidity() && this.submit() }"}
    (h2/raw (anti-forgery-field))
    [:fieldset {:class "sm:px-4 py-2 sm:border-2 border-slate-200 rounded"}
     [:legend {:class "text-lg font-semibold leading-tight sm:px-1"} "Edit client"]
     (text-field :name "Name" (:name client) (:name errors))
     (text-field :short_name "Short name" (:short_name client) (:short_name errors))
     (text-field :entity_name "Business entity name" (:entity_name client) (:entity_name errors))
     (text-field :abn "ABN" (:abn client) (:abn errors))
     (checkbox-field :active "Active" (:active client) (:active errors))
     (date-field :commenced "Commenced" (:commenced client) (:commenced errors))
     (textarea-field :summary "Summary" (:summary client) (:summary errors) "20rem")
     (textarea-field :background "Background" (:background client) (:background errors) "10rem")
     (textarea-field :people "People" (:people client) (:people errors) "10rem")
     (textarea-field :service "Service arrangements" (:service client) (:service errors) "10rem")
     (textarea-field :technology "Technology" (:technology client) (:technology errors) "10rem")
     (checkbox-field :service_agreement "Service agreement (PFA)" (:service_agreement client) (:service_agreement errors))
     (checkbox-field :retainer "Retainer (PFW)" (:retainer client) (:retainer errors))
     (checkbox-field :pro_bono "Pro bono" (:pro_bono client) (:pro_bono errors))
     (checkbox-field :risky "Risky" (:risky client) (:risky errors))
     (checkbox-field :non_financial "Non-financial" (:non_financial client) (:non_financial errors))
     [:div.mb-2
      (f/label {:class "block"} :current_value "Current value ")
      (f/drop-down :current_value [["-" ""]["low" "low"] ["medium" "medium"] ["high" "high"]] (:current_value client))
      (when-let [e (:current_value errors)] [:div.notification.is-danger e])]
     [:div.mb-2
      (f/label {:class "block"} :potential_value "Potential value ")
      (f/drop-down :potential_value [["-" ""]["low" "low"] ["medium" "medium"] ["high" "high"]] (:potential_value client))
      (when-let [e (:potential_value errors)] [:div.notification.is-danger e])]
     (range-field :rating_people "People" (:rating_people client) (:rating_people errors))
     (range-field :rating_profit "Profit" (:rating_profit client) (:rating_profit errors))
     (range-field :rating_culture "Culture" (:rating_culture client) (:rating_culture errors))
     (range-field :rating_values "Values" (:rating_values client) (:rating_values errors))
     [:div.mb-2
      (f/label {:class "block"} :year_turnover "Year turnover ")
      (f/text-field {:type :number :min 0 :class "px-1 border-2 border-slate-300 rounded w-full"} :year_turnover (:year_turnover client))
      (when-let [e (:year_turnover errors)] [:div.notification.is-danger e])]
     [:div.mb-2
      (f/label {:class "block"} :lifetime_value "Lifetime value ")
      (f/text-field {:type :number :min 0 :class "px-1 border-2 border-slate-300 rounded w-full"} :lifetime_value (:lifetime_value client))
      (when-let [e (:lifetime_value errors)] [:div.notification.is-danger e])]
     [:div [:button {:type :submit
                     :class "button h-10 px-6 mb-2 font-semibold rounded-md bg-slate-200 border-2 border-slate-300"} "Save"]]]]))

(defn edit-client
  [{:keys [params request-method] :as request}]
  (let [{:keys [query-fn]} (utils/route-data request)
        client-id (get-in request [:parameters :path :id])
        client (query-fn :get-client {:id client-id})]
    (if (= request-method :post)
      (let [[errors data] (st/validate params client-schema2 {:strip true})]
        (if (nil? errors)
          (do
            (query-fn :update-client! (assoc data :id client-id))
            (http/found (str "/client/" client-id)))
          (edit-client-view client-id data errors)))
      (edit-client-view client-id client {}))))

(comment
  (require '[selmer.parser :as parser])
  (parser/render "{{d|date:mediumDate}}" {:d (java.util.Date.)})
  )
