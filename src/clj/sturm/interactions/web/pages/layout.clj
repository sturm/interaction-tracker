(ns sturm.interactions.web.pages.layout
  "HTML template and Selmer utilities."
  (:require
   [clojure.java.io]
   [clojure.string :as str]
   [markdown.core :as md]
   [markdown.transformers :as md-tr]
   [selmer.filters :as filters]
   [selmer.parser :as parser]
   [ring.util.http-response :refer [content-type ok]]
   [ring.util.anti-forgery :refer [anti-forgery-field]]
   [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
   [ring.util.response]
   [clj-time.core :as t]
   [clj-time.coerce :as coerce]))

(def selmer-opts {:custom-resource-path (clojure.java.io/resource "html")})

(defn escape-html
  "Change special characters into HTML character entities.
  Used by :markdown filter."
  ;; TODO: What about preventing other HTML that could break the layout?
  ;; TODO: What about HTMX attributes that can run arbitrary code?
  ;; TODO: What about Markdown features we don't want, eg. H1. Really only want
  ;; paragraphs, lists, bold, italic and links.
  [text state]
  [(if-not (or (:code state) (:codeblock state))
     (-> text
         (clojure.string/replace "<script" "")
         (clojure.string/replace "javascript:" ""))
     text) state])

;; Hacked up from https://stackoverflow.com/a/32511406, with added coercion to
;; handle java.sql.Date. Possibly should just avoid using clj-time though. This
;; simple function is fairly course - doesn't give you part of years eg. "2
;; years ago" rather than "2.4 years ago".
;;
;; Could potentially switch to clj-commons/humanize.
(defn time-ago [time]
  (let [fixed-time (coerce/from-date time)
        units [{:name "second" :limit 60 :in-second 1}
               {:name "minute" :limit 3600 :in-second 60}
               {:name "hour" :limit 86400 :in-second 3600}
               {:name "day" :limit 604800 :in-second 86400}
               {:name "week" :limit 2629743 :in-second 604800}
               {:name "month" :limit 31556926 :in-second 2629743}
               {:name "year" :limit Long/MAX_VALUE :in-second 31556926}]
        diff (try
               (t/in-seconds (t/interval fixed-time (t/now)))
               (catch IllegalArgumentException _
                 (* -1 (t/in-seconds (t/interval (t/now) fixed-time)))))]
    (if (< (abs diff) 5)
      "just now"
      (let [unit (first (drop-while #(or (>= (abs diff) (:limit %))
                                         (not (:limit %)))
                                    units))]
        (-> (/ (abs diff) (:in-second unit))
            Math/floor
            int
            (#(str (when (neg? diff) "-") % " " (:name unit) (when (> % 1) "s") " ago")))))))

(comment
  (time-ago (java.sql.Date. 1999999999999))
)

(defn markdown-no-html
  "Escaping prevents any literal HTML being passed through.
  Only issue with this approach is that it causes URLs like
  <http://www.example.com> to be escaped rather than hyperlinked."
  [content]
  [:safe (md/md-to-html-string content :replacement-transformers (into [escape-html] md-tr/transformer-vector))])

(defn thousands
  "Display currency in a compact form, eg. $134K, $19.6K, $788."
  [n & [decimal-places]]
  ;;(filters/throw-when-expecting-number n)
  (cond
    (nil? n) ""
    (> n 100000) (format "%.0fK" (/ (double n) 1000))
    (> n 1000) (format (str "%." (if decimal-places decimal-places "1") "fK") (/ (double n) 1000))
    (zero? n) 0
    :else n))

(defn display-name
  [client]
  (let [short-name (:short_name client)
        name (:name client)]
    (if (empty? short-name) name short-name)))

(defn init-selmer!
  []
  (parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))
  (filters/add-filter! :markdown markdown-no-html)
  (filters/add-filter! :thousands thousands)
  (filters/add-filter! :display-name display-name)
  (filters/add-filter! :time-ago time-ago))

(defn render
  [_request template & [params]]
  (-> (parser/render-file template
                          (assoc params :page template :csrf-token *anti-forgery-token*)
                          selmer-opts)
      (ok)
      (content-type "text/html; charset=utf-8")))

(defn error-page
  "error-details should be a map containing the following keys:
   :status - error status
   :title - error title (optional)
   :message - detailed error message (optional)
   returns a response map with the error page as the body
   and the status specified by the status key"
  [error-details]
  {:status  (:status error-details)
   :headers {"Content-Type" "text/html; charset=utf-8"}
   :body    (parser/render-file "error.html" error-details selmer-opts)})
