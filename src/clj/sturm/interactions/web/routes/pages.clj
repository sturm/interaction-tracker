(ns sturm.interactions.web.routes.pages
  (:require
   [sturm.interactions.web.middleware.exception :as exception]
   [sturm.interactions.web.middleware.auth :as auth]
   [sturm.interactions.web.pages.layout :as layout]
   [sturm.interactions.web.controllers.interactions :as interactions]
   [integrant.core :as ig]
   [reitit.coercion.spec]
   [reitit.coercion]
   [reitit.ring.coercion :as rrc]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.parameters :as parameters]
   [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
   [selmer.middleware]))

(defn wrap-page-defaults []
  (let [error-page (layout/error-page
                     {:status 403
                      :title "Invalid anti-forgery token"})]
    #(wrap-anti-forgery % {:error-response error-page})))

;; Routes
(defn page-routes [_opts]
  [["/" {:get interactions/client-overview}]
   ["/search" {:get interactions/site-search-fragment}]
   ["/save-client" {:post interactions/save-client!}]
   ["/client/:id" {:get interactions/view-client
                   :parameters {:path {:id int?}}}]

   ["/interactions" {:get interactions/interaction-list-fragment}]
   ["/client/:id/edit" {:get interactions/edit-client
                        :post interactions/edit-client
                        :parameters {:path {:id int?}}}]
   ["/add-interaction/:client-id" {:get interactions/add-interaction!
                                   :post interactions/add-interaction!
                                   :parameters {:path {:client-id int?}}}]
   ["/interaction/:id" {:get interactions/view-update-interaction!
                        :post {:handler interactions/view-update-interaction!}
                        :parameters {:path {:id int?}}}]])

(def wrap-spec-error
  "Reitit returns a spec error rather than a 404 if parameter validation fails.
  It's up to us if we want to handle that better."
  {:name ::wrap-spec-error
   :wrap (fn [handler]
           (fn [request]
             (let [response (handler request)]
               (if (get-in response [:body :problems])
                 {:status 404 :body "Path invalid" :headers {"Content-Type" "text/html"}}
                 response))))})

(defn route-data [opts]
  (merge
   opts
   {:coercion reitit.coercion.spec/coercion
    :middleware
    [;; Default middleware for pages
     (wrap-page-defaults)
     ;; query-params & form-params
     parameters/parameters-middleware
     ;; encoding response body
     muuntaja/format-middleware
     ;; exception handling
     exception/wrap-exception

     ;; Add helpful template compilation error HTML output
     ;; TODO: Should be dev-only
     selmer.middleware/wrap-error-page
     ;; Auth with Buddy
     auth/wrap-auth
     auth/wrap-restricted
     ;; add :parameters key to request with cooerce path, query and body parameters
     wrap-spec-error
     rrc/coerce-exceptions-middleware
     rrc/coerce-request-middleware]}))

(derive :reitit.routes/pages :reitit/routes)

(defmethod ig/init-key :reitit.routes/pages
  [_ {:keys [base-path]
      :or   {base-path ""}
      :as   opts}]
  (layout/init-selmer!)
  [base-path (route-data opts) (page-routes opts)])
