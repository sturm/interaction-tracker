(ns sturm.interactions.web.routes.utils)

(def route-data-path [:reitit.core/match :data])

;; Seems to be used only for extracting the :query-fn out of the request.
(defn route-data
  [req]
  (get-in req route-data-path))

;; Not used at all.
(defn route-data-key
  [req k]
  (get-in req (conj route-data-path k)))
