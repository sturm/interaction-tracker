(ns sturm.interactions.web.middleware.auth
  (:require
   ;; [buddy.auth.backends.session :as session]
   [buddy.auth.backends :as backends]
   [buddy.auth :as auth]
   [buddy.auth.accessrules :as accessrules]
   [buddy.auth.middleware :as auth-middleware]))

(defn on-error [request _response]
  {:status 401
   :headers {"www-authenticate" "Basic realm=\"Interaction Tracker\""}
   :body (str "Access to " (:uri request) " is not authorized")})

(defn wrap-restricted [handler]
  (accessrules/restrict handler {:handler auth/authenticated?
                                 :on-error on-error}))

(defn authfn
  [_request authdata]
  (let [username (:username authdata)
        password (:password authdata)]
    (if (and (= username (System/getenv "AUTH_USER"))
             (= password (System/getenv "AUTH_PASSWORD")))
      username
      false)))

(defn wrap-auth [handler]
  (let [
        ;; backend (session/session-backend)
        backend (backends/basic {:realm "Interaction Tracker"
                                 :authfn authfn})]
    (-> handler
        (auth-middleware/wrap-authentication backend)
        (auth-middleware/wrap-authorization backend))))
