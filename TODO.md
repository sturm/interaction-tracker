* more keyboard-driven - like Org-capture
* track approximate response time eg. 1-4 hours, 1 day, 2-4 days, 1 week
* look at mobile form field styling for Django admin
* reopen the last added/edited interaction <details> when the list is reloaded
* Interactions per month (last full month), 12 month average
* Dates/times are in local timezone
* How to parse URL parameters directly to integers
* Consider using ring-defaults to automatically rewrite based on scheme (rather
  than doing it via current Nginx proxy-rewrite config)
  https://github.com/ring-clojure/ring-defaults?tab=readme-ov-file#proxies
* Chart all client value
* Handle form 403 (presumably if CSRF token has expired)
* Track unplanned incoming calls/SMSs (interruptions)
* CSS cache-busting
* User accounts
* Version number
* Demo to Karl/Monica
* Keyword filtering of interactions
* Archive a client
* Delete an interaction
* Would be awesome to have a JS lib that would let you annotate a webpage with ideas. Would need to generate a sensible CSS path or similar to the relevant element, possibly simplifying it by climbing up if you're the first child.
* indicator while add/update request is in flight (for slow/poorly connected mobile)
* Rebuild in Django (and possibly other things like Biff, Compojure or Rails for comparison)
* Go back and calculate Boojum lifetime value
* Add a "short" name for use in charts
* Hyperlink value chart items
* Try Hiccup - I think it's the ultimate thing - really what I like Crispy Forms for
* Flag for when an interaction requires a follow-up - list on dashboard
* Track contractual arrangements - their contract or mine, duration, renewal, notice required
* Have a copy of current service agreement attached to client
* Sparklines showing interactions per month and revenue per year
* nice 404
* selmer errors only in dev
* understand all the middleware in use
* 500 handler for dev/prod
* try creating a Django version for comparison
