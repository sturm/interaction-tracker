(ns sturm.interactions.env
  (:require [clojure.tools.logging :as log]))

#_:clj-kondo/ignore
(def defaults
  {:init       (fn []
                 (log/info "\n-=[interactions starting]=-"))
   :start      (fn []
                 (log/info "\n-=[interactions started successfully]=-"))
   :stop       (fn []
                 (log/info "\n-=[interactions has shut down successfully]=-"))
   :middleware (fn [handler _] handler)
   :opts       {:profile :prod}})
