(ns sturm.interactions.env
  (:require
    [clojure.tools.logging :as log]
    [sturm.interactions.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init       (fn []
                 (log/info "\n-=[interactions starting using the development or test profile]=-"))
   :start      (fn []
                 (log/info "\n-=[interactions started successfully using the development or test profile]=-"))
   :stop       (fn []
                 (log/info "\n-=[interactions has shut down successfully]=-"))
   :middleware wrap-dev
   :opts       {:profile       :dev
                :persist-data? true}})
