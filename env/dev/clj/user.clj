(ns user
  "Userspace functions you can run by default in your local REPL."
  (:require
    [clojure.pprint]
    [clojure.spec.alpha :as s]
    [clojure.tools.namespace.repl :as repl]
    ;; [criterium.core :as c]                                  ;; benchmarking
    [expound.alpha :as expound]
    [integrant.core :as ig]
    [integrant.repl :refer [;; clear
                            go
                            halt
                            ;; prep
                            ;; init
                            reset
                            ;; reset-all
                            ]]
    [integrant.repl.state :as state]
    ;; [kit.api :as kit]
    [lambdaisland.classpath.watch-deps :as watch-deps]      ;; hot loading for deps
    [selmer.parser]
    [sturm.interactions.core]  ;; Must be required so init-key methods run
    [sturm.interactions.config]))

;; uncomment to enable hot loading for deps
(watch-deps/start! {:aliases [:dev :test]})

(alter-var-root #'s/*explain-out* (constantly expound/printer))

(add-tap (bound-fn* clojure.pprint/pprint))

(defn dev-prep!
  []
  (integrant.repl/set-prep! (fn []
                              (-> (sturm.interactions.config/system-config {:profile :dev})
                                  (ig/prep)))))

(defn test-prep!
  []
  (integrant.repl/set-prep! (fn []
                              (-> (sturm.interactions.config/system-config {:profile :test})
                                  (ig/prep)))))

;; Can change this to test-prep! if want to run tests as the test profile in your repl
;; You can run tests in the dev profile, too, but there are some differences between
;; the two profiles.
(dev-prep!)

(repl/set-refresh-dirs "src/clj")

(def refresh repl/refresh)

(selmer.parser/cache-off!)


(comment
  (go)
  (reset)
  (halt)

  (require '[reitit.core :as r])

  (let [router (:router/core state/system)]
    (:data (r/match-by-path router "/client/99")))

  (let [router (:router/core state/system)]
    (:path-params (r/match-by-name router :sturm.interactions.web.routes.pages/client {:id 99})))

  ((:handler/ring state/system) {:request-method :get :uri "/client/6a"})

  ;; Need to re-run this after (reset) to load modified queries
  (def query-fn (:db.sql/query-fn state/system))

  (query-fn :total-interactions-annual {:client-id 1})

  (query-fn :save-client! {:name "Test"})
  (query-fn :get-clients {})

  (query-fn :search-interactions {:search "crowdspot"})

  (query-fn
   :update-interaction!
   {:id 40
    :timestamp #inst "2017-08-23T10:22:22"
    :subject "Test client called about launch"
    :medium "phone"
    :duration 15
    :details "We agreed to launch Thursday afternoon."
    :internal-notes ""})
  (query-fn :get-interactions {})

  (require '[migratus.core])
  (migratus.core/create
   (:db.sql/migrations state/system)
   "add-client-year-turnover-field")
  )
