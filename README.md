# Interaction Tracker

Start a [REPL](#repls) in your editor or terminal of choice.

Start the server with:

```clojure
(go)
```

The default API is available under http://localhost:3000/api

System configuration is available under `resources/system.edn`.

To reload changes:

```clojure
(reset)
```

## Tailwind

`npx tailwind --input=tailwind/input.css --output=resources/public/outp
ut.css --watch`

`npx tailwind --input=tailwind/input.css --output=resources/public/outp
ut.css --minify
`

## REPLs

### Cursive

Configure a [REPL following the Cursive documentation](https://cursive-ide.com/userguide/repl.html). Using the default "Run with IntelliJ project classpath" option will let you select an alias from the ["Clojure deps" aliases selection](https://cursive-ide.com/userguide/deps.html#refreshing-deps-dependencies).

### CIDER

Use the `cider` alias for CIDER nREPL support (run `clj -M:dev:cider`). See the [CIDER docs](https://docs.cider.mx/cider/basics/up_and_running.html) for more help.

Note that this alias runs nREPL during development. To run nREPL in production (typically when the system starts), use the kit-nrepl library through the +nrepl profile as described in [the documentation](https://kit-clj.github.io/docs/profiles.html#profiles).

### Command Line

Run `clj -M:dev`, `clj -M:dev:nrepl` or `make repl`.

Note that, just like with [CIDER](#cider), this alias runs nREPL during development. To run nREPL in production (typically when the system starts), use the kit-nrepl library through the +nrepl profile as described in [the documentation](https://kit-clj.github.io/docs/profiles.html#profiles).


## How it works

The challenge with using a framework like Kit is that while the extra
indirection, build process and dev/test/prod are useful as you grow, they make
it harder to understand how the system really fits together and what code kicks
it all off.

Here's how to run the system in production-mode, without using the build process:

```
JDBC_URL='jdbc:postgresql:///interaction_dev?user=[USER]&password=[PASSWORD]' clojure -Sdeps '{:aliases {:prod {:extra-paths ["src/clj" "resources" "env/prod/resources" "env/prod/clj"]}}}' -A:prod --main sturm.interactions.core
```

Here we extend the Java classpath to include `src/clj`, `resources`,
`env/prod/resources` and `env/prod/clj` (`src/clj` is technically included by
default, but it helps to be explicit). We then run the "main" function in
`sturm.interactions.core` namespace. Instead of `--main sturm.interactions.core`,
we could equivalently run `-X sturm.interactions.core/-main`.
