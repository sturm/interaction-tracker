;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

(specifications->manifest
  (list
   ;; Using OpenJDK 17 above because that's what is on torino.sturm.com.au
   ;; (Debian Bookworm). Using OpenJDK 21 fails with:
   ;; "ClassNotFoundException: java.util.SequencedCollection".
   ;;
   ;; I used to think I had to use the "jdk" output here, as in "openjdk@17:jdk"
   ;; but testing now it only seems I need the JRE from default output, as in
   ;; "openjdk@17".
   "openjdk@17"

   ;; Does *not* work with clojure-tools from Guix as of July 2024. Instead
   ;; install Clojure as per https://clojure.org/guides/install_clojure, using:
   ;; ./linux-install.sh --prefix ~/.local
   ;;
   ;; The issue I've had with clojure-tools from Guix is that it doesn't find all
   ;; the transitive dependencies for io.github.kit-clj/kit-undertow, like
   ;; org.jboss.xnio/xnio-nio. It worked ok when I explicitly added all these to
   ;; deps.clj.
   ;;
   ;; To work around this, I have the Clojure command line tools installed from
   ;; https://clojure.org/releases/tools into ~/.local/bin.
   ;;
   ;; "clojure-tools"

   ;; Needed by `clj`
   "rlwrap"

   ;; Linting
   "clj-kondo"

   ;; For deployment with vps-deploy
   "python"
   "curl"

   ;; For building Tailwind CSS
   "node@18"
  ))
