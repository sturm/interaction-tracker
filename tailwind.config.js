/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./tailwind/**/*.{html,js}",
    "./resources/html/**/*.html"],
  theme: {
    extend: {},
  },
  plugins: [],
}
