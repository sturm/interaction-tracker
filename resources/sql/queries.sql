-- :name save-client! :insert :affected
-- :doc creates a new clients
INSERT INTO client
(name)
VALUES (:name)

-- :name update-client! :execute :affected
-- :doc creates a new clients
UPDATE client
set name = :name, short_name = :short_name, entity_name = :entity_name, abn = :abn, active = :active, commenced = :commenced,
summary = :summary, background = :background, service = :service, people = :people, technology = :technology,
service_agreement = :service_agreement, retainer = :retainer, risky = :risky,
pro_bono = :pro_bono,
non_financial = :non_financial,
current_value = :current_value, potential_value = :potential_value,
year_turnover = :year_turnover, lifetime_value = :lifetime_value,
rating_people = :rating_people, rating_profit = :rating_profit,
rating_culture = :rating_culture, rating_values = :rating_values
WHERE id = :id

-- :name get-clients :query :many
-- :doc selects all available clients
SELECT client.*,
(service_agreement or retainer) as commitment,
count(interaction.id) as total_interactions,
bool_or(interaction.followup_required) as followup_required
FROM client left outer join interaction on interaction.client_id = client.id group by client.id order by year_turnover desc, lifetime_value desc

-- :name get-client :query :one
-- :doc selects all fields for a client
SELECT client.*, count(interaction.id) as total_interactions
FROM client left outer join interaction on interaction.client_id = client.id
where client.id = :id
group by client.id

-- :name save-interaction! :returning-execute
-- :doc creates a new interaction
INSERT INTO interaction
(client_id, subject, medium, duration, details, internal_notes, followup_required)
VALUES (:client-id, :subject, :medium, :duration, :details, :internal-notes, :followup-required)
RETURNING id

-- :name update-interaction! :execute :affected
-- :doc updates an interaction
UPDATE interaction
SET timestamp = :timestamp, subject = :subject, medium = :medium, duration = :duration, details = :details, internal_notes = :internal-notes, followup_required = :followup-required
WHERE id = :id

-- :name get-interactions :query :many
-- :doc selects all available interactions for a client
SELECT * FROM interaction where client_id = :client-id order by timestamp desc

-- :name total-interactions-annual :query :one
SELECT COUNT(*) FROM interaction where client_id = :client-id AND timestamp > CURRENT_DATE - interval '1 year'

-- :name search-interactions-by-client :query :many
-- :doc selects all available interactions for a client
SELECT interaction.*
FROM interaction
WHERE
client_id = :client-id
AND to_tsvector('english', subject || ' ' || details || ' ' || internal_notes) @@ plainto_tsquery(:query)
ORDER BY timestamp DESC

-- :name search-interactions :query :many
-- :doc finds all available interactions matching query
SELECT interaction.*, client.name, client.short_name
FROM interaction
INNER JOIN client ON client_id = client.id
WHERE
-- TODO: Not entirely happy with client name search - inefficient
to_tsvector('english', client.name) @@ plainto_tsquery(:query)
OR to_tsvector('english', client.short_name) @@ plainto_tsquery(:query)
OR to_tsvector('english', subject || ' ' || details || ' ' || internal_notes) @@ plainto_tsquery(:query)
ORDER BY timestamp DESC LIMIT 20

-- :name recent-interactions :query :many
-- :doc selects the latest interactions across all clients
SELECT interaction.*, client.name, client.short_name FROM interaction inner join client on client_id = client.id order by timestamp desc limit 10

-- :name get-interaction :query :one
-- :doc selects all fields for a client
SELECT * FROM interaction where id = :id
