CREATE TABLE client
(id SERIAL primary key,
 name varchar(50));
--;;
CREATE TABLE interaction
(id SERIAL primary key,
 client_id integer references client(id) NOT NULL,
 timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 subject character varying(255) NOT NULL,
 medium character varying(50) NOT NULL,
 duration smallint,
 details text,
 internal_notes text);
