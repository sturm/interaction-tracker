alter table client
add column rating_people smallint not null default 3,
add column rating_profit smallint not null default 3,
add column rating_culture smallint not null default 3,
add column rating_values smallint not null default 3;
