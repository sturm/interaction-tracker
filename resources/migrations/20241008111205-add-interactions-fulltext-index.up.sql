CREATE INDEX interaction_fulltext ON interaction USING GIN (to_tsvector('english', subject || ' ' || details || ' ' || internal_notes))
